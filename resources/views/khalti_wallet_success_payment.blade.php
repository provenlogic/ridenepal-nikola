<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Khalti Payment Result</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css">
        	.header
        	{
				font-size: 21px;
			    font-weight: 700;
			    font-family: sans-serif;
			    padding-top: 10px;
			    padding-bottom: 10px;
        	}
        </style>
     
    </head>
    <body>
        <nav class="navbar navbar-default" 
        	style="background-color: #0c3d99;color: white;border-color: #0c3d99;border-radius: 0px;">
		  <div class="container-fluid">
		  	<div class="header" style="text-align:center">Khalti Wallet Payment Confirmation</div>
		  </div>
		</nav>
		<div class="row">
			<div class="col-md-3"></div>			
			<div class="col-md-6 col-sm-12 col-xs-12">

					<div style="text-align: center;font-size: 32px;color: green;">
						<i class="fa fa-check-circle"></i>
						Success !!
					</div>
				
					<div style="padding: 25px;">
						<label>Payment Details</label>
						<table class="table table-bordered">
						    <tbody>
						    	<tr>
						        <td>Name</td>
						        <td>{{$user->first_name}} {{$user->last_name}}</td>
						      </tr>
						      <tr>
						        <td>Email</td>
						        <td>{{$user->email}}</td>
						      </tr>
                              <tr>
						        <td>Payment ID</td>
						        <td>{{$transaction->trans_id}}</td>
						      </tr>
						      <tr>
						        <td>Transaction Date</td>
						        <td>{{$transaction->created_at}}</td>
						      </tr>
						      <tr>
						        <td>Amount</td>
						        <td>{{$transaction->amount}}</td>
						      </tr>
					
						    </tbody>
						  </table>

					</div>

					<div style="text-align: center;">
                        <button id="close_btn"  class="btn btn-success" type="button" value="done" onclick="ok.performClick(this.value);">Done!!</button>
					</div>

                    <div style="text-align: center;margin-top: 15px;">Note : Take a screenshot of this page for future use if any problem occurs.</div>

			</div>			
			<div class="col-md-3"></div>			
		</div>
    </body>
</html>