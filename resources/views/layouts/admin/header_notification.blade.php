<div class="dropdown navbar-custom-menu">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
        <i class="fa fa-inbox" style="font-size:35px;"></i>
        @if($unread_notifications_count)
        <span class="badge label-danger" id="notificion_count_badge" style="position: absolute;left: 0;top: 0;">{{$unread_notifications_count}}</span>
        @endif
    </button>
    <ul class="dropdown-menu" style="right:0px;left:initial;border: 1px solid #00000040;box-shadow: 0 1px 6px 0 rgba(32,33,36,0.28);">
        <li style="text-align:right;"> <a style="display:inline-block" href="javascript:void(0);" title="Click to clear all" onclick="clearAllNotificaitons()">Clear All</a></li>
        @foreach($notifications as $notification)
            @if(in_array($notification->type, ['new_user_register', 'new_provider_register']))
            <li class="notificaiton_item">
                <a href="{{json_decode($notification->data)->url_to_open}}">{{$notification->message}}</a>
                @if(!$notification->is_read)
                <a class="mark_read_btn" id="mark_read_btn_{{$notification->id}}" href="javascript:void(0);" title="Click to mark read" onclick="markRead({{$notification->id}})">Mark Read</a>
                @endif
            </li>
            @endif
            @if(in_array($notification->type, ['provider_not_found']))
            <li class="notificaiton_item">
                <a href="{{json_decode($notification->data)->url_to_open}}">{{$notification->message}}</a>
                @if(!$notification->is_read)
                <a class="mark_read_btn" id="mark_read_btn_{{$notification->id}}" href="javascript:void(0);" title="Click to mark read" onclick="markRead({{$notification->id}})">Mark Read</a>
                @endif
            </li>
            @endif
            @if(in_array($notification->type, ['new_outstation_request_booked']))
            <li class="notificaiton_item">
                <a href="{{json_decode($notification->data)->url_to_open}}">{{$notification->message}}</a>
                @if(!$notification->is_read)
                <a class="mark_read_btn" id="mark_read_btn_{{$notification->id}}" href="javascript:void(0);" title="Click to mark read" onclick="markRead({{$notification->id}})">Mark Read</a>
                @endif
            </li>
            @endif
        @endforeach
    </ul>
</div>