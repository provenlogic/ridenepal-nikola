<style>
    .notificaiton_item {
        padding: 5px;
        border: 1px solid #00000038;
        margin: 5px;
        box-shadow: 0 1px 6px 0 rgba(32,33,36,0.28);
        overflow: auto;
    }
    .mark_read_btn {
        display: inline-block !important;
        padding: 0 !important;
        font-size: 10px;
        float: right;
        color: black !important;
        position: relative;
    }
</style>
<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">{{Setting::get('site_name')}}</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{{Setting::get('site_name')}}</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </a>
        <span id="header_notificaiton_continer">
            
        </span>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="{{route('admin.profile')}}" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="@if(Auth::guard('admin')->user()->picture){{Auth::guard('admin')->user()->picture}} @else {{asset('admin-css/dist/img/avatar.png')}} @endif" class="user-image" alt="User Image">
                    <span class="hidden-xs">{{Auth::guard('admin')->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="@if(Auth::guard('admin')->user()->picture){{Auth::guard('admin')->user()->picture}} @else {{asset('admin-css/dist/img/avatar.png')}} @endif" class="img-circle" alt="User Image">
                            <p>
                                {{Auth::guard('admin')->user()->name}}
                                <small>Admin</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('admin.profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('admin.logout')}}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<script>

function clearAllNotificaitons()
{
    $.get("{{route('notifications.clear')}}", function(){
        $("body #notificion_count_badge").hide();
    });
}

function markRead(id)
{
    $.get('{{route('notifications.mark.read')}}?id='+id, function(response){

        let currentNumber = parseInt($("body #notificion_count_badge").text());
        currentNumber--;
        $("body #notificion_count_badge").text(currentNumber);
        if(currentNumber == 0) {
            $("body #notificion_count_badge").hide();
        }

        $("body #mark_read_btn_"+id).remove();

    });
}

var notificationViewApi = "{{route('notifications.view')}}";
function loadNotificationView() {
    
    $.get(notificationViewApi, function(response){

        $("#header_notificaiton_continer").html(response);

    });

}
setInterval(() => {
    loadNotificationView();
}, 10000);

window.onload = function() {
    loadNotificationView();
}


</script>