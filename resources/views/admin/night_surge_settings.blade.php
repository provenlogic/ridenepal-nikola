@extends('layouts.admin')
@section('content-header', "Night Surge Settings")
@section('content')
@include('notification.notify')
<style></style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <div class="map_content">
                    <p class="lead para_mid">
                        This page contains settings for night surge price, and from-to time
                    </p>
                </div>
            </div>
            <div class="box-body">
                
            <form class="" action="{{route('admin.nightsurge.settings.save')}}">
                <div class="form-group">
                    <label for="from_time">From Time</label>
                    <input type="time" class="form-control" id="from_time" name="from_time" required value="{{$fromtime}}">
                </div>
                <div class="form-group">
                    <label for="to_time">To Time</label>
                    <input type="time" class="form-control" id="to_time" name="to_time" required value="{{$totime}}">
                </div>
                <div class="form-group">
                    <label for="price_amount">Flat Price Amount / Percentage</label>
                    <input type="number" class="form-control" id="price_amount" name="price_amount" required min="0" step="1" value="{{$amount}}">
                </div>
                <div class="form-group">
                    <label for="surge_type">Surge Type</label>
                    <select name="surge_type" id="surge_type">
                        <option value="flat" @if($surge_type == 'flat') selected @endif>By Flat Amount</option>
                        <option value="percentage" @if($surge_type == 'percentage') selected @endif>By Percentage Amount</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="night_surge_enabled ">Enable/Disable</label>
                    <select name="night_surge_enabled" id="night_surge_enabled">
                        <option value="true" @if($night_surge_enabled == 'true') selected @endif>Enabled</option>
                        <option value="false" @if($night_surge_enabled == 'false' || $night_surge_enabled == '') selected @endif>Disabled</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
            </form>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript"></script>
@endsection