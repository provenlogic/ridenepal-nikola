@extends('layouts.admin')
@section('content-header', 'Wallet')
@section('content')
@include('notification.notify')
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
    .balance {
    font-size:24px;
    }
    .balance-currency-symbol {
    vertical-align:super;
    }
    #transaction_id_refresh {
        cursor: pointer;
        color: green;
        position: absolute;
        top: 0;
        right: 0px;
        font-size: 33px;
    }
    #remarks-char-left {
    /* position: absolute; */
    font-size: 10px;
    right: 0;
    }
</style>
@if(in_array("2", explode(",", Auth::guard("admin")->user()->user_wallets)))
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 style="margin: 0;">Recharge User Wallet</h3>
            </div>
            <div class="box-body">
                <form id="recharge-form" method="POST" action="{{route('admin.user.wallets.recharge.save')}}">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{$user->id}}" name="user_id">
                    <div class="row clearfix">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <b>Name</b>
                                <div class="form-line">
                                    <input type="text" class="form-control" value="{{$user->first_name . ' ' . $user->last_name}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <b>Email</b>
                                <div class="form-line">
                                    <input type="text" required class="form-control" placeholder="" value="{{$user->email}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" >
                            <b>Transaction Id</b>
                            <div class="form-group" style="position:relative">
                                <input type="text" class="form-control" name="transaction_id" style="padding-right: 30px;">
                                <i class="material-icons" id="transaction_id_refresh" title="Generate transaction id">refresh</i>
                            </div>
                        </div>
                        <div class="col-sm-3 ">
                            <b>Current balance</b>
                            <div class="form-group">
                                <span class="balance-currency-symbol"></span>
                                <span class="balance current-balance" >{{$user->wallet}}</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <b>Action</b><br>
                                <input name="action_type" type="radio" id="credit" class="with-gap radio-col-red" checked value="credit">  
                                <label for="credit">Credit</label>
                                <input name="action_type" type="radio" id="debit" class="with-gap radio-col-red" value="debit">  
                                <label for="debit">Debit</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <b>Amount() to credit or debit</b>
                                <div class="form-line">
                                    <input type="number" min="0" required class="form-control" placeholder="" value="0.00" name="amount" step=".01" onblur="this.value=parseFloat(this.value).toFixed(2)">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <b>Closing balance will be</b>
                            <div class="form-group">
                                <span class="balance-currency-symbol"></span>
                                <span class="balance closing-balance">{{$user->wallet}}</span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <b>Remarks(256 characters)</b>
                                <div class="form-line">
                                    <input type="text" required class="form-control" placeholder="" name="remarks" value="">                                        
                                </div>
                                <span id="remarks-char-left">Chars left: 256</span>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-12 text-right">
                            <button type="submit" id="" class="btn btn-primary waves-effect">
                            <span>SAVE</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@if(in_array("3", explode(",", Auth::guard("admin")->user()->user_wallets)))
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 style="margin: 0;">Recharge Histories</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Transaction Id</th>
                            <th>Amount</th>
                            <th>Closing Amout</th>
                            <th>Remarks</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $tr)
                        <tr>
                            <td>{{$tr->trans_id}}</td>
                            <td>{{$tr->amount}}</td>
                            <td>{{$tr->closing_amount}}</td>
                            <td>{{$tr->remarks}}</td>
                            <td>{{$tr->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
<script type="text/javascript">

    /** returns unique transaction id */
    function generateTransactionId() {
        let spart = Math.random().toString(36).substr(2, 10);
        let epart = Math.random().toString(36).substr(2, 5);
        return `${spart}${epart}`.toUpperCase();
    }
    /** create and set new transaction id */
    function setNewTransactionId()
    {   
        let transid = generateTransactionId();
        $("input[name=transaction_id]").val( transid );
    }
    /** calculate and set closing balance */
    function calculateAndSetClosingBalance()
    {
        let currentBalance = parseFloat($(".current-balance").text());
        let amount = parseFloat($("input[name=amount]").val())
        let action = $('input[name=action_type]:checked').val();
        let closingBalance = 0;
        if(action == 'credit') {
            closingBalance = currentBalance + amount;
        } else {
            closingBalance = currentBalance - amount;
        }
        $(".closing-balance").text(closingBalance.toFixed(2));
    }



    window.onload = function(){
    
        $("input[name=remarks]").on('keyup', function(){
            let charleft = 256 - $(this).val().length;
            $('#remarks-char-left').text(charleft);
        });


        setNewTransactionId();
        $("#transaction_id_refresh").on('click', setNewTransactionId);
        $("input[name=amount]").on('blur', calculateAndSetClosingBalance).on('focus', function(){
            $(this).select();
        }).on('keyup', calculateAndSetClosingBalance);
        $('input[name=action_type]').on('change', calculateAndSetClosingBalance);
    
    
    }
    
</script>
@endsection