@extends('layouts.admin')
@section('content-header', 'Ride Requests Chat Messages')
@section('content')
@include('notification.notify')
<style>
    .messages-header h1 {
        padding: 0;
        margin: 0;
    }

    .messages-body {
        overflow:auto;
    }

    .messages-header {
        border-bottom: 1px solid #0000002e;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }

    .message-container {
        border: 1px solid #0000003d;
        padding: 15px;
    }

    .message {
        /* padding: 5px; */
        overflow: auto;
    }

    .left-message {
        float: left;
        max-width: 80%;
        min-width:40%;
        border-radius: 3px;
        padding: 5px;
        margin-bottom: 5px;
        width:80%;
    }

    .right-message {
        float: right;
        border-radius: 3px;
        padding: 5px;
        margin-bottom: 5px;
        max-width: 80%;
        min-width:40%;
        width:80%;
        text-align:right;
    }

</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <div class="map_content">
                    <p class="lead para_mid">
                        This page contains chat message for request id : {{$requests->id}}
                    </p>
                </div>
            </div>
            <div class="box-body">
                
                @if(!$user || !$provider || !count($messages))
                    No Messages Found
                @else


                    <div class="message-container">
                        <div class="messages-header">
                            <h1>Messages</h1>
                        </div>
                        <div class="messages-body">


                            @foreach($messages as $message)

                                @if($message->type == 'up')

                                <div class="message">
                                    <div class="alert-success  left-message">
                                        <div>{{$message->message}}</div>
                                        <div>User : {{$user->email}} . Time : {{$message->created_at}}</div>
                                    </div>
                                </div>

                                @elseif($message->type == 'pu')
                                <div class="message">
                                    <div class="alert-danger right-message">
                                        <div>{{$message->message}}</div>
                                        <div>Provider: {{$provider->email}} . Time : {{$message->created_at}}</div>
                                    </div>
                                </div>
                                @endif

                            @endforeach
                        </div>
                    </div>


                @endif

            </div>
        </div>
    </div>
</div>
<script type="text/javascript"></script>
@endsection