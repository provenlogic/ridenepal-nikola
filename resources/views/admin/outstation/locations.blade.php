@extends('layouts.admin')
@section('content-header', "Outstation Locations and Add New Location")
@section('content')
@include('notification.notify')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY_FOR_NIKOLA')}}&libraries=places"></script>
<style>
    .box-header {
    border-bottom: 1px solid #00000017;
    }
    .fa-trash {
    color:red;
    }
    .table tr td {
    width:1%;
    }
</style>
@if(in_array("2", explode(",", Auth::guard("admin")->user()->outstation_location)))
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Outstation Settings
            </div>
            <div class="box-body">
                <form class="" method="POST" action="{{route('admin.outstation.assign.settings.save')}}">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="from_time">Auto Assign Process</label>
                            <br>
                            <input type="checkbox" name="is_oustation_assign_process_auto" @if($is_oustation_assign_process_auto == 1) checked @endif>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="from_time">Assign Process Starts After Minute</label>
                            <input type="text" class="form-control" name="outstation_assgin_auto_process_starts_after_min" required value="{{$outstation_assgin_auto_process_starts_after_min}}">
                        </div>
                    </div>
                    @if(in_array("1", explode(",", Auth::guard("admin")->user()->outstation_location)))
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="from_time">&nbsp;</label>
                            <button type="submit" class="btn btn-success btn-block">Save</button>
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@if(in_array("4", explode(",", Auth::guard("admin")->user()->outstation_location)))
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Add New Outstation Location
            </div>
            <div class="box-body">
                <form class="" method="POST" action="{{route('admin.outstation.location.save')}}">
                    <input type="hidden" name="latitude">
                    <input type="hidden" name="longitude">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="from_time">City</label>
                            <input type="text" class="form-control" name="location_city" id="location_city" required value="" autocomplete="off" onblur="this.value = ucfirst(this.value)">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="from_time">State</label>
                            <input type="text" class="form-control" name="location_state" required value="" onblur="this.value = ucfirst(this.value)">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="from_time">Country</label>
                            <input type="text" class="form-control" name="location_country" required value="" onblur="this.value = ucfirst(this.value)">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="from_time">Country Short Code</label>
                            <input type="text" class="form-control" name="location_country_short" required value="" onblur="this.value = ucfirst(this.value)">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Outstation Locations
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($locations as $location)
                        <tr>
                            <td>{{$location->city}}</td>
                            <td>{{$location->state}}</td>
                            <td>{{$location->country}}-{{$location->country_short}}</td>
                            <td>
                                @if(in_array("5", explode(",", Auth::guard("admin")->user()->outstation_location)))
                                <a style="color:red" href="{{route('admin.outstation.location.delete', ['location_id' => $location->id])}}">&lt;&lt;Delete&gt;&gt;</a>
                                &nbsp;&nbsp;
                                @endif
                                <a href="Javascript:void(0)" data-toggle="collapse" data-target="#outstations_{{$location->id}}">&lt;&lt;Outstations&gt;&gt;</a>
                            </td>
                        </tr>
                        <tr  id="outstations_{{$location->id}}" class="collapse" >
                            <td colspan="4">
                                <div class="box box-info" style="border-top: none;">
                                    <div class="box-header">From {{$location->city}} To All Destinations</div>
                                    <div class="box-body table-responsive">
                                        @if(in_array("4", explode(",", Auth::guard("admin")->user()->outstation_location)))
                                        <form method="POST" class="form-inline" action="{{route('admin.outstation.location.destination.add', [ 'location_id' => $location->id ])}}" style="padding:8px;border: 1px solid #00000014;">
                                            <div class="form-group">
                                                <label>Destination Location : </label>
                                                <select name="destination_location_id">
                                                    @foreach($locations as $childLocation)
                                                        @if($childLocation->id == $location->id)
                                                            @continue
                                                        @endif
                                                    <option value="{{$childLocation->id}}">{{$childLocation->city}}, {{$childLocation->state}}, {{$childLocation->country}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-success btn-xs" style="vertical-align: baseline;">Add</button>
                                        </form>
                                        @endif
                                        <table class="table table-hover table-bordered ">
                                            <thead>
                                                <tr>
                                                <th>City</th>
                                                <th>State</th>
                                                <th>Country</th>
                                                <th>Distance(km)</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($location->destinations as $dLocation)
                                                <tr>
                                                    <td>{{$dLocation->destination->city}}</td>
                                                    <td>{{$dLocation->destination->state}}</td>
                                                    <td>{{$dLocation->destination->country}}-{{$dLocation->destination->country_short}}</td>
                                                    <td>{{$dLocation->distance}}</td>
                                                    <td>
                                                        <a style="color:red" href="{{route('admin.outstation.location.destination.delete', [ 'source_location_id' => $location->id, 'destination_location_id' => $dLocation->destination->id ])}}">&lt;&lt;Delete&gt;&gt;</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function ucfirst(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    
    
    
    function placeChangeHandler() {
    
        place = autocomplete.getPlace();
        console.log('place', place);
    
        let city = '', state = '', country = '', country_short = '';
    
        place.address_components.forEach(function(component) {
            var types = component.types;
            if (types.indexOf('locality') > -1) {
                city = component.long_name;
            }
    
            if (types.indexOf('administrative_area_level_1') > -1) {
                state = component.long_name;
            }
    
            if (types.indexOf("country") > -1) {
                country = component.long_name;
            }
    
            if (types.indexOf("country") > -1) {
                country_short = component.short_name;
            }

            $("input[name=latitude]").val( place.geometry.location.lat() );
            $("input[name=longitude]").val( place.geometry.location.lng() );


        });
    
        console.log('city, state, country country_short', city, state, country, country_short)
    
        $("#location_city").val(city);
        $("input[name=location_state]").val(state);
        $("input[name=location_country]").val(country);
        $("input[name=location_country_short]").val(country_short);
    }
    
    var autocomplete;
    function initialize() {
    
        var options = {
            types: ['(cities)']
        };
    
        var input = document.getElementById('location_city');
        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', placeChangeHandler);
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
    
</script>
@endsection