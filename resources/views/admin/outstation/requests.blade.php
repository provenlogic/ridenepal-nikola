@extends('layouts.admin')
@section('content-header', "Outstation Requests")
@section('content')
@include('notification.notify')
<style>
    .box-header {
    border-bottom: 1px solid #00000017;
    }
    .fa-trash {
    color:red;
    }
    .table tr td {
    /* width:1%; */
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Outstation Requests
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped" id="outstation_requests">
                    <thead>
                        <tr>
                            <th>Booking Id</th>
                            <th>Source</th>
                            <th>Destination</th>
                            <th>Status</th>
                            <th>No. Biddings</th>
                            <th>Round Trip</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Estimated Total</th>
                            <th>Bid Amount</th>
                            <th>Provider</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($outstationRequests as $request)
                        <tr>
                            <td>{{$request['booking_id']}}</td>
                            <td>{{$request['source_address']}}</td>
                            <td>{{$request['destination_address']}}</td>
                            <td>{{$request['status_text']}}</td>
                            <td>@if($request['status'] == 'bidding') {{$request['no_of_biddings']}} @else N/A @endif</td>
                            <td>@if($request['is_round_trip']) Yes @else No @endif</td>
                            <td>{{$request['trip_start_datetime']}}</td>
                            <td>@if($request['is_round_trip']) {{$request['trip_end_datetime']}} @else N/A @endif</td>
                            <td>{{$request['total']}}</td>
                            <td>{{$request['provider_bid_amount']}}</td>
                            <td>@if(!$request['provider']) N/A @else {{$request['provider']->first_name . ' ' . $request['provider']->last_name}} @endif</td>
                            <td><a href="{{route('admin.outstation.request.details.show', [ 'request_id' => $request['id'] ])}}">View</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
window.onload = function(){
    
    $('#outstation_requests').DataTable({
        "paging": true,
        "autoWidth": false,
        "aaSorting": [],
        'iDisplayLength': 100
    });
}
</script>
@endsection