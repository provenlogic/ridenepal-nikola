@extends('layouts.admin')
@section('content-header', "Outstation Request Details")
@section('content')
@include('notification.notify')
<style>
    .box-header {
    border-bottom: 1px solid #00000017;
    }
    .fa-trash {
    color:red;
    }
    .table tr td {
    /* width:1%; */
    }
    .green-text {
        color:green;
    }

    .red-text {
        color:red;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Booking Details
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Booking Id</th>
                            <td>{{$outstationRequestData['booking_id']}}</td>
                        </tr>
                        <tr>
                            <th>User</th>
                            <td>{{$outstationRequestData['user']['first_name'] . ' ' . $outstationRequestData['user']['last_name']}}</td>
                        </tr>
                        @if($outstationRequestData['provider'])
                        <tr>
                            <th>Assigned Provider</th>
                            <td>{{$outstationRequestData['provider']['first_name'] . ' ' . $outstationRequestData['provider']['last_name']}}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Source</th>
                            <td>{{$outstationRequestData['source_address']}}</td>
                        </tr>
                        <tr>
                            <th>Destination</th>
                            <td>{{$outstationRequestData['destination_address']}}</td>
                        </tr>
                        <tr>
                            <th>Service</th>
                            <td>{{$outstationRequestData['service']['name']}}</td>
                        </tr>
                        <tr>
                            <th>Round Trip</th>
                            <td>@if($outstationRequestData['is_round_trip']) Yes @else No @endif</td>
                        </tr>
                        <tr>
                            <th>Start Date</th>
                            <td>{{$outstationRequestData['trip_start_datetime']}}</td>
                        </tr>
                        @if($outstationRequestData['is_round_trip'])
                        <tr>
                            <th>End Date</th>
                            <td>{{$outstationRequestData['trip_end_datetime']}}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Estimated Price</th>
                            <td>{{$outstationRequestData['estimated_price']}}</td>
                        </tr>
                        <tr>
                            <th>Provider Bid</th>
                            <td>{{$outstationRequestData['provider_bid_amount']}}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>{{$outstationRequestData['status_text']}}</td>
                        </tr>
                        <tr>
                            <th>Distance (km)</th>
                            <td>{{$outstationRequestData['distance']}}</td>
                        </tr>
                        <tr>
                            <th>Created On</th>
                            <td>{{$outstationRequestData['created_at']}} . {{$outstationRequestData['time_ago']}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@if(!empty($invoiceArray))
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Invoice
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Payment Id</th>
                            <td>{{$invoiceArray['payment_id']}}</td>
                        </tr>
                        <tr>
                            <th>Payment Mode</th>
                            <td>{{ucfirst($invoiceArray['payment_mode'])}}</td>
                        </tr>
                        <tr>
                            <th>Time Taken (minutes)</th>
                            <td>{{$invoiceArray['total_time']}} minute</td>
                        </tr>
                        <tr>
                            <th>Distance Traveled (km)</th>
                            <td>{{$invoiceArray['distance_travel']}} km</td>
                        </tr>
                        <tr>
                            <th>Base Price</th>
                            <td>{{$invoiceArray['base_price']}}</td>
                        </tr>
                        <tr>
                            <th>Booking Fee</th>
                            <td>{{$invoiceArray['booking_fee']}}</td>
                        </tr>
                        <tr>
                            <th>Distance Price</th>
                            <td>{{$invoiceArray['distance_price']}}</td>
                        </tr>
                        <tr>
                            <th>Driver Allowance</th>
                            <td>{{$invoiceArray['driver_allowance']}}</td>
                        </tr>
                        <tr>
                            <th>Cancellation Fine</th>
                            <td>{{$invoiceArray['cancellation_fine']}}</td>
                        </tr>
                        <tr>
                            <th>Bonus Discount</th>
                            <td>-{{$invoiceArray['bonus_discount']}}</td>
                        </tr>
                        <tr>
                            <th>Promo Discount</th>
                            <td>-{{$invoiceArray['promo_value']}}</td>
                        </tr>
                        <tr>
                            <th>Tax</th>
                            <td>{{$invoiceArray['tax_price']}}</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <td>{{$invoiceArray['total']}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                Biddings
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped">
                    <thead>
                        <th>Provider</th>
                        <th>Rating</th>
                        <th>Bid Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach($biddings as $bidding)
                        <tr>
                            <td>{{$bidding->provider->first_name . ' ' . $bidding->provider->last_name}}</td>
                            <td>{{$bidding->rating}}</td>
                            <td>{{$bidding->bidding_amount}}</td>
                            <td @if($bidding->status == 'assigned') class="green-text" @endif @if($bidding->status == 'rejected') class="red-text" @endif>{{$bidding->status_text}}</td>
                            <td>
                                @if($bidding->status == 'accepted')
                                    <a href="{{route('admin.outstation.request.assign', [ 'request_id' => $bidding->outstation_request_id, 'bidding_id' => $bidding->id ])}}">Assign Provider</a>
                                @else
                                N/A
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
</script>
@endsection