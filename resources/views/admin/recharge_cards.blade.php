@extends('layouts.admin')
@section('content-header', 'Recharge Cards')
@section('content')
@include('notification.notify')
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
    .action-btn {
        color: red;
        font-size: 20px;
        cursor: pointer;
    }
</style>
@if(in_array("1", explode(",", Auth::guard("admin")->user()->recharge_cards)))
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 style="margin: 0;">Add New Recharge Card</h3>
            </div>
            <div class="box-body">
                <form class="form-inline" action="{{route('admin.recharge-cards.generate')}}" method="POST">
                    <input type="hidden" value="{{csrf_token()}}" />
                    <div class="form-group">
                        <label for="number_of_cards">No of Cards : </label>
                        <input type="number" class="form-control" id="number_of_cards" name="number_of_cards" value="1" min="1">
                    </div>
                    &nbsp;
                    <div class="form-group">
                        <label for="amount">Amount : </label>
                        <input type="number" class="form-control" id="amount" name="amount" value="0.00" min="0.00">
                    </div>
                    &nbsp;
                    <button type="submit" class="btn btn-primary">GENERATE</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 style="margin: 0;">List Recharge Cards</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="recharge-card-table">
                    <thead>
                        <tr>
                            <th>Pin</th>
                            <th>Amount</th>
                            <th>Used</th>
                            <th>Created On</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rCards as $card)
                        <tr id="row_{{$card->id}}">
                            <td>{{$card->pin}}</td>
                            <td>{{$card->amount}}</td>
                            <td>
                                @if($card->is_used) 
                                    Used
                                @else 
                                    Not Used
                                @endif
                            </td>
                            <td>{{$card->created_at}}</td>
                            <td>
                                @if(in_array("3", explode(",", Auth::guard("admin")->user()->recharge_cards)))
                                <i class="material-icons action-btn delete-btn" data-card-id="{{$card->id}}">delete</i>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @if(!count($rCards)) 
                        <tr>
                            <td colspan="5" class="text-center">No recharge cards found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

window.onload = function(){

    $('#recharge-card-table').DataTable({
        "paging": true,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });

    var deleteCardApi = "{{route('admin.recharge-cards.delete')}}";
    $(".delete-btn").on('click', function(){

        let id = $(this).data('card-id');

        $.post(deleteCardApi, {card_id : id}, function(response){
            $("#row_"+id).fadeOut();
        });


    });
    
    $('input[name=amount]').on('blur', function(){
        let newval = parseFloat($(this).val()).toFixed(2);
        $(this).val( newval )

    });

    $('input[name=amount]').on('focus', function(){
        $(this).select();
    })


}

</script>
@endsection