@extends('layouts.admin')
@section('content-header', 'User Wallets')
@section('content')
@include('notification.notify')
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
    .action-btn {
        color: red;
        font-size: 20px;
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 style="margin: 0;">User Wallets</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="user-wallet-table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Wallet Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr id="row_{{$user->id}}">
                            <td>{{$user->id}}</td>
                            <td>{{$user->first_name . ' ' . $user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->wallet}}</td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" class="btn btn-xs btn-primary" href="{{route('admin.user.wallets.recharge.show', ['user_id' => $user->id])}}">Recharge</a>
                                    <a type="button" class="btn btn-xs btn-primary" href="{{route('admin.user.wallets.recharge.show', ['user_id' => $user->id])}}">Transactions</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

window.onload = function(){

   $('#user-wallet-table').DataTable({
        "paging": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        'iDisplayLength': 100
    });

}

</script>
@endsection