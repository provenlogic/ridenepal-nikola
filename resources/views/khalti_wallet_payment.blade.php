<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://khalti.com/static/khalti-checkout.js"></script>
</head>
<body style="margin:0px;">
    
    <h1 style="text-align:center">Payment is being processed...</h1>
    
    <script>
        var config = {
            // replace the publicKey with yours
            "publicKey": "{{$pubickey}}",
            "productIdentity": "{{$productidentity}}",
            "productName": "Wallet Recharge",
            "productUrl": "http://104.248.135.98",
            "eventHandler": {
                onSuccess (payload) {
                    // hit merchant api for initiating verfication
                    console.log(payload);

                    window.location.href= "{{url('khalti/wallet/payment/verify')}}?user_id={{$userid}}&amount={{$amount}}&token=" + payload.token;

                },
                onError (error) {
                    console.log(error);
                },
                onClose () {
                    window.location.href = "{{url('khalti/payment/error')}}"
                }
            }
        };

        var checkout = new KhaltiCheckout(config);
        checkout.show({amount: {{$amount}} });
    </script>
    <!-- Paste this code anywhere in you body tag -->
</body>
</html>