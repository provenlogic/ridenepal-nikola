<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistanceAndTimeToOutstationLocationDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outstaion_location_destinations', function (Blueprint $table) {
            $table->double('distance', 15,2)->default(0.00)->comment('total distance in km');
        });

        Schema::table('outstation_locations', function (Blueprint $table) {
            $table->double('latitude',15,8);
            $table->double('longitude',15,8);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outstaion_location_destinations', function (Blueprint $table) {
            //
        });
    }
}
