<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutstationLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstation_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('city', 128);
            $table->string('state', 128);
            $table->string('country', 128);
            $table->string('country_short', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outstation_locations');
    }
}
