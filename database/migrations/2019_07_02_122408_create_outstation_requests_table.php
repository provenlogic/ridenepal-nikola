<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutstationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstation_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('booking_id', 128);
            $table->bigInteger('user_id')->unsigned();
            
            $table->string('source_address', 256);
            $table->double('source_latitude',15,8);
            $table->double('source_longitude',15,8);
            $table->string('destination_address', 256);
            $table->double('destination_latitude',15,8);
            $table->double('destination_longitude',15,8);

            $table->bigInteger('service_id')->unsigned()->default(0);
            $table->bigInteger('provider_id')->unsigned()->default(0);
            
            $table->decimal('provider_bid_amount', 10, 2)->default(0.00);
            $table->decimal('driver_allowance', 10, 2)->default(0.00);
            $table->decimal('base_fare', 10, 2)->default(0.00);
            $table->decimal('booking_fee', 10, 2)->default(0.00);
            $table->decimal('distance_price', 10, 2)->default(0.00);
            $table->decimal('tax', 10, 2)->default(0.00);
            $table->decimal('total', 10, 2)->default(0.00);
            $table->integer('days')->default(0)->comment('today duration in days');
            $table->integer('distance')->default(0)->comment('total distance in km');

            $table->decimal('promo_discount', 10, 2)->default(0.00);
            $table->string('promo_code', 128)->default('');
            $table->string('payment_mode', 128)->default('');
            $table->string('payment_status', 128)->default('');
            $table->string('status', 128)->default('');
            $table->string('transaction_id', 128)->default('');


            $table->boolean('is_round_trip')->default(false);
            $table->dateTime('trip_start_datetime');
            $table->dateTime('trip_end_datetime')->nullable();
            $table->dateTime('start_time');
            $table->dateTime('end_time');

            $table->string('start_otp', 10);
            $table->string('end_otp', 10);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outstation_requests');
    }
}
