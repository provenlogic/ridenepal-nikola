<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutstationBiddingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstation_biddings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('outstation_request_id')->unsigned();
            $table->bigInteger('provider_id')->unsigned();
            $table->decimal('bidding_amount', 10, 2)->default(0.00);
            $table->string('status', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outstation_biddings');
    }
}
