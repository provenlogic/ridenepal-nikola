<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutstaionLocationDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstaion_location_destinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('source_location_id')->unsigned();
            $table->bigInteger('destination_location_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outstaion_location_destinations');
    }
}
