<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OutstationLocation;
use App\OutstationRequest;
use App\OutstationLocationDestination;
use App\ServiceType;
use App\Settings;
use Validator;
use App\Provider;
use App\Helpers\Helper;
use App\UserRating;
use App\User;
use App\AdminNotification;

class UserOutstation extends Controller
{

    public function __construct()
    {
        $this->middleware('UserApiVal' , [ 'except' => ['getOutstationSourceLocations', 'getOutstationDestinationLocations', 'getOutstationServicePrices'] ]);
    }


    /**
     * cancel booking only before driver has not started
     */
    public function cancelBooking(Request $request)
    {
        $bRecord = OutstationRequest::where('user_id', $request->id)
        ->whereIn('status', ['pending', 'bidding', 'driver_assigned'])
        ->where('id', $request->booking_id)
        ->first();


        $bRecord->status = 'user_canceled';
        $bRecord->save();

        $provider = $bRecord->provider;
        if($provider) {
            Helper::send_notifications($provider->id, 2, 'Oustation Booking Canceled', "Outstation booking id : {$bRecord->booking_id} is canceled.");
        }

        return response()->json([ 'success' => true, 'message' => 'booking canceled successfully'], 200);

    }




    /** get booking details by id */
    public function getOutstationBookingDetails(Request $request)
    {
        $bRecord = OutstationRequest::where('user_id', $request->id)
        //->whereIn('status', ['pending', 'bidding', 'driver_assigned'])
        ->where('id', $request->booking_id)
        ->first();


        $booking = [
            'id' => $bRecord->id,
            'booking_id' => $bRecord->booking_id,
            'service_name' => ServiceType::where('id', $bRecord->service_id)->first()->name,
            'service_picture' => ServiceType::where('id', $bRecord->service_id)->first()->picture,
            'source' => $bRecord->source_address,
            'destination' => $bRecord->destination_address,
            'start_date' => $bRecord->trip_start_datetime,
            'end_date' => $bRecord->trip_end_datetime,
            'round_trip' => $bRecord->is_round_trip,
            'map' => Helper::getStaticMapImage($bRecord->source_latitude, $bRecord->source_longitude, $bRecord->destination_latitude, $bRecord->destination_longitude),
            'start_otp' => $bRecord->start_otp,
            'end_otp' => $bRecord->end_otp,
            'status' => $bRecord->status,
            'status_text' => $bRecord->status_text,
        ];

        $provider = $bRecord->provider;
        $provider_details = [
            'name' => $provider ? $provider->first_name . ' ' . $provider->last_name :  "",
            'email' => $provider ? $provider->email :  "",
            'mobile' => $provider ? $provider->mobile :  "",
        ];

        $vehicle_details = [
            'plate_no' => $provider ? $provider->plate_no :  "",
            'model' => $provider ? $provider->model :  "",
            'color' => $provider ? $provider->color :  "",
            'car_image' => $provider ? $provider->car_image :  "",
        ];


        $invoice = [
            'base_fare' => $bRecord->base_fare,
            'booking_fee' => $bRecord->booking_fee,
            'distance_price' => $bRecord->distance_price,
            'tax' => $bRecord->tax,
            'total' => $bRecord->total,
            'payment_mode' => $bRecord->payment_mode,
            'distance' => $bRecord->distance,
            'time' => Helper::getTravelTime($bRecord->source_latitude, $bRecord->source_longitude, $bRecord->destination_latitude, $bRecord->destination_longitude)
        ];

        $responseArray = array_merge($booking, $provider_details, $vehicle_details, $invoice);

        return response()->json([ 'success' => true, 'message' => 'booking fetched successfully', 'booking' => $responseArray ], 200);


    }



    /** get out station bookings */
    public function getOutstationBookings(Request $request)
    {
        $bookingRecords = OutstationRequest::where('user_id', $request->id)
        ->whereIn('status', ['pending', 'bidding', 'driver_assigned'])
        ->orderBY('trip_start_datetime', 'desc')
        ->get();


        $bookings = [];
        foreach($bookingRecords as $record) {
            $tempRecord = [
                'id' => $record->id,
                'booking_id' => $record->booking_id,
                'date' => $record->trip_start_datetime,
                'source_address' => $record->source_address,
                'destination_address' => $record->destination_address,
                'round_trip' => $record->is_round_trip,
                'amount' => $record->total,
                'status' => $record->status,
                'status_text' => $record->status_text,
                'service' => ServiceType::where('id', $record->service_id)->first()->name
            ];
            $bookings[] = $tempRecord;
        }

        return response()->json([ 'success' => true, 'message' => 'bookings fetched successfully', 'bookings' => $bookings ], 200);
    }


    /** retry booking if no providers found 
     * change booking status to pending if no_provider_found
    */
    public function retryOutstationBooking(Request $request)
    {
        /** fetch bookign record if status is no_provider_found */
        $booking = OutstationRequest::where('id', $request->booking_id)->where('user_id', $request->id)->where('status', 'no_provider_found')->first();

        if(!$booking) {
            return response()->json([ "success" => false, 'error_code' => 1, 'message' => "Your are not allowed to retry" ], 200);
        }

        $booking->status = 'pending';
        $booking->save();
        
        return response()->json([ 'success' => true, 'message' => 'Your ourstation request has been send. In a while we will assign driver for you. Thanks.', 'request_details' => $booking ], 200);
    }

    

    /** get destination locations by city */
    public function getOutstationDestinationLocations(Request $request)
    {

        $city = OutstationLocation::where('city', $request->city)->first();
        $destinations = OutstationLocationDestination::where('source_location_id', $city->id)->get();  
       
        $destinationIds = $destinations->pluck('destination_location_id');
        $locations = OutstationLocation::whereIn('id', $destinationIds)
        ->orderBy('city')
        ->select(['id', 'city', 'state', 'country', 'country_short'])
        ->get();  
        
        $locations->map(function($item) use($destinations) {
            $item['distance'] = $destinations->where('destination_location_id', $item->id)->first()->distance;
        });

        return response()->json([ 'success' => true, 'locations' => $locations->toArray() ], 200);
    }



    /** get outstation locations */
    public function getOutstationSourceLocations()
    {
        $locations = OutstationLocation::orderBy('city')
        ->select(['id', 'city', 'state', 'country', 'country_short'])
        ->get();   
        
        return response()->json([ 'success' => true, 'locations' => $locations->toArray() ], 200);
    }


    /** get service prices */
    public function getOutstationServicePrices(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'service_id' => 'sometimes|required|exists:service_types,id',            
            'distance' => 'required|numeric',
            'round_trip' => 'required|boolean',
            'trip_start_datetime' => 'required_if:round_trip,1|date_format:Y-m-d H:i:s',
            'trip_end_datetime' => 'required_if:round_trip,1|date_format:Y-m-d H:i:s',
        ]);


        if($validator->fails()) {
            return response()->json([ "success" => false, 'error_code' => 1, 'message' => collect($validator->errors()->getMessages())->flatten()[0] ], 200);
        }


        
        $serviceid = $request->service_id;
        $distance = $request->distance;
        $round_trip = $request->round_trip == 1 ? true : false;
        $currency = Settings::getByKey('currency');

        $serviceList = Helper::calculatePrice($serviceid, $distance, $round_trip, $request->trip_start_datetime, $request->trip_end_datetime);

        return response()->json([ 
            'success' => true, 
            'currency' => $currency, 
            'services' => $serviceList
        ], 200);

    }



    



    /** book out station service */
    public function bookOutstation(Request $request)
    {

        $user = User::find($request->id);

        /** add user payment mode to request */
        $request->request->add([ 'payment_mode' => $user->payment_mode ]);

        
        $validator = Validator::make($request->all(), [
            'payment_mode' => 'required|in:cod,wallet,esewa,khalti',
            'service_id' => 'required|exists:service_types,id',
            'source_address' => 'required|max:256',
            'source_latitude' => "required|numeric",
            'source_longitude' => "required|numeric",
            'destination_address' => 'required|max:256',
            'destination_latitude' => "required|numeric",
            'destination_longitude' => "required|numeric",
            'distance' => 'required|numeric',
            'round_trip' => 'required|boolean',
            'trip_start_datetime' => 'required|date_format:Y-m-d H:i:s',
            'trip_end_datetime' => 'required_if:round_trip,1|date_format:Y-m-d H:i:s',
        ]);


        if($validator->fails()) {
            return response()->json([ "success" => false, 'error_code' => 1, 'message' => collect($validator->errors()->getMessages())->flatten()[0] ], 200);
        }

        /** fetch price */
        $price = Helper::calculatePrice(
            $request->service_id, 
            $request->distance, 
            $request->round_trip == 1, 
            $request->trip_start_datetime, 
            $request->trip_end_datetime
        )[0];

        /** create out station request */
        $outstationRequest = new OutstationRequest;
        $outstationRequest->service_id = $request->service_id;
        $outstationRequest->booking_id = Helper::randomChars(10); //generate random booking id
        $outstationRequest->user_id = $user->id;
        $outstationRequest->provider_id = 0; //dont assign provider

        $outstationRequest->source_address = $request->source_address;
        $outstationRequest->source_latitude = $request->source_latitude;
        $outstationRequest->source_longitude = $request->source_longitude;
        $outstationRequest->destination_address = $request->destination_address;
        $outstationRequest->destination_latitude = $request->destination_latitude;
        $outstationRequest->destination_longitude = $request->destination_longitude;

        $outstationRequest->is_round_trip = $request->round_trip;
        $outstationRequest->trip_start_datetime = $request->trip_start_datetime;
        $outstationRequest->trip_end_datetime = $request->trip_end_datetime;

        $outstationRequest->start_otp = rand(1000, 9999);
        $outstationRequest->end_otp = rand(1000, 9999);

        $outstationRequest->payment_mode = $request->payment_mode;
        $outstationRequest->payment_status = 'not_paid';
        $outstationRequest->base_fare = $price['base_fare'];
        $outstationRequest->driver_allowance = $price['driver_allowance'];
        $outstationRequest->booking_fee = $price['booking_fee'];
        $outstationRequest->distance_price = $price['distance_price'];
        $outstationRequest->tax = $price['tax'];
        $outstationRequest->total = $price['total'];
        $outstationRequest->days = $price['days'];
        $outstationRequest->distance = $price['distance'];

        $outstationRequest->status = 'pending';
        $outstationRequest->save();
        

        /** send admin panel notification */
        $aNotification = new AdminNotification;
        $aNotification->type = 'new_outstation_request_booked';
        $aNotification->message = "{$user->first_name} booked for outstaion, Booking Id : {$outstationRequest->booking_id}";
        $aNotification->data = json_encode([
            'url_to_open' => route('admin.outstation.request.details.show', ['request_id' => $outstationRequest->id])
        ]);
        $aNotification->save();


        return response()->json([ 'success' => true, 'message' => 'Your ourstation request has been send. In a while we will assign driver for you. Thanks.', 'request_details' => $outstationRequest ], 200);
        

    }





}
