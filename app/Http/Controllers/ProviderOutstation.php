<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OutstationRequest;
use App\OutstationBidding;
use App\Jobs\NormalPushNotification;
use App\Jobs\sendPushNotification;
use Validator;
use App\Helpers\Helper;
use App\ProviderRating;
use App\Requests;


class ProviderOutstation extends Controller
{

    public function __construct()
    {
        $this->middleware('ProviderApiVal' , ['except' => [] ]);
    }


    /** provider started to reach user location 
     * copy all details to request table
    */
    public function providerStarted(Request $request)
    {
        $booking = OutstationRequest::whereIn('status', ['driver_assigned'])
            ->where('provider_id', $request->id)
            ->where('id', $request->booking_id)
            ->first();

        if(!$booking) {
            return response()->json([ "success" => false, 'error_code' => 1, 'message' => "You are not allowed to start outstation booking" ], 200);
        }

        $requests = new Requests;
        $requests->request_type = $booking->service_id;
        $requests->status = 2;
        $requests->provider_status = 2;
        $requests->user_id = $booking->user_id;
        $requests->confirmed_provider = $booking->provider_id;
        $requests->promo_code = $booking->promo_code;
        $requests->request_start_time = date("Y-m-d H:i:s");
        $requests->s_address = $booking->source_address;
        $requests->s_latitude = $booking->source_latitude;
        $requests->s_longitude = $booking->source_longitude;
        $requests->d_address = $booking->destination_address;
        $requests->d_latitude = $booking->destination_latitude;
        $requests->d_longitude = $booking->destination_longitude;
        $requests->request_status_type = 4; //outstation request
        $requests->start_otp = $booking->start_otp;
        $requests->end_otp = $booking->end_otp;
        $requests->outstation_request_id = $booking->id;

        $requests->save();


        /** start the outstaion trip */
        $booking->status = 'provider_started';
        $booking->save();


        /** make provider unavailable */
        $provider = $booking->provider;
        $provider->is_available = 0;
        $provider->save();

        // Send Push Notification to User
        $title = Helper::tr('provider_started_title');
        $message = Helper::tr('provider_started_message');
        dispatch( new sendPushNotification($requests->user_id, 1, $requests->id, $title, $message, ''));


        return response()->json([ 'success' => true, 'message' => 'Request started successfully', 'request' => $requests ], 200);


    }



    /** give rating to user for outstation request */
    // public function rateUser(Request $request)
    // {
    //     $booking = OutstationRequest::whereIn('status', ['ended'])
    //     ->with(['providerRating'])
    //     ->where('provider_id', $request->id)
    //     ->where('id', $request->booking_id)
    //     ->first();


    //     if($booking->providerRating) {
    //         return response()->json([ "success" => false, 'error_code' => 1, 'message' => "Rating already given." ], 200);
    //     }

    //     /** insert rating for provider */
    //     $rating = new ProviderRating;
    //     $rating->provider_id = $request->id;
    //     $rating->user_id = $booking->user_id;
    //     $rating->request_id = $booking->id;
    //     $rating->rating = $request->rating;
    //     $rating->comment = $request->comments ?: '';
    //     $rating->request_type = 'outstation';
    //     $rating->save();
      
    //     return response()->json([ 'success' => true, 'message' => 'User rated successfully.'], 200);


    // }





    /** end outstation booking */
    // public function endOutstationBooking(Request $request)
    // {
    //     $booking = OutstationRequest::whereIn('status', ['started'])
    //     ->where('provider_id', $request->id)
    //     ->where('id', $request->booking_id)
    //     ->where('end_otp', $request->end_otp)
    //     ->first();

    //     if(!$booking) {
    //         return response()->json([ "success" => false, 'error_code' => 1, 'message' => "Your are not allowed to end or end otp is wrong" ], 200);
    //     }
        
    //     /** start the outstaion trip */
    //     $booking->status = 'ended';
    //     $booking->end_time = date('Y-m-d H:i:s');

    //     /** if payment mode is cod, then make paid */
    //     if($booking->payment_mode == 'cod') {
    //         $booking->transaction_id = Helper::randomChars(16);
    //         $booking->payment_status = 'paid';
    //     }

    //     $booking->save();

    //     /** make provider available */
    //     $provider = $booking->provider;
    //     $provider->is_available = 1;
    //     $provider->save();


    //     dispatch( new NormalPushNotification($booking->user_id, 0, "Outstaion Ended", "Your outstation booking id : {$booking->booking_id} ended. Pay amount : {$booking->total}") );

    //     return response()->json([ 'success' => true, 'message' => 'Outstation booking ended successfully.'], 200);

    // }



    /** start outstation booking */
    // public function startOutstationBooking(Request $request)
    // {
    //     $booking = OutstationRequest::whereIn('status', ['driver_assigned'])
    //     ->where('provider_id', $request->id)
    //     ->where('id', $request->booking_id)
    //     ->where('start_otp', $request->start_otp)
    //     ->first();

    //     if(!$booking) {
    //         return response()->json([ "success" => false, 'error_code' => 1, 'message' => "Your are not allowed to start or start otp is wrong" ], 200);
    //     }
        
    //     /** start the outstaion trip */
    //     $booking->status = 'started';
    //     $booking->start_time = date('Y-m-d H:i:s');
    //     $booking->save();

    //     /** make provider un available */
    //     $provider = $booking->provider;
    //     $provider->is_available = 0;
    //     $provider->save();

    //     dispatch( new NormalPushNotification($booking->user_id, 0, "Outstaion Started", "Your outstation booking id : {$booking->booking_id} started") );

    //     return response()->json([ 'success' => true, 'message' => 'Outstation booking started successfully.'], 200);

    // }





    /**
     * get outstation bookings
     */
    public function getOutstationBookings(Request $request)
    {
        $bookings = OutstationRequest::whereIn('status', ['driver_assigned', 'user_canceled', 'driver_canceled', 'canceled', 'started', 'ended'])
            ->where('provider_id', $request->id)
            ->with([ 
                'user' => function($query) {
                    $query->select(['id', 'first_name', 'last_name', 'email', 'mobile']);
                }
            ])
            ->orderBy('trip_start_datetime', 'desc')
            ->get();

        return response()->json([ 'success' => true, 'message' => 'Outstation bookings fetched', 'bookings' => $bookings ], 200);
    }





    /** accept outstation request */
    public function acceptOutstationRequest(Request $request)
    {
        $outstationRequest = OutstationBidding::where('status', 'pending')->where('provider_id', $request->id)->where('id', $request->request_id)->first();

        if(!$outstationRequest || $request->bidding_amount < 1) {
            return response()->json([ "success" => false, 'error_code' => 1, 'message' => "Your are not allowed to accept or bidding amount missing" ], 200);
        }

        $outstationRequest->status = 'accepted';
        $outstationRequest->bidding_amount = $request->bidding_amount;
        $outstationRequest->save();

        return response()->json([ 'success' => true, 'message' => 'Outstation request bidding completed successfully.'], 200);
    }




    /**
     * reject outstation reqeust
     */
    public function rejectOutstationRequest(Request $request)
    {
        $outstationRequest = OutstationBidding::where('status', 'pending')->where('provider_id', $request->id)->where('id', $request->request_id)->first();

        if(!$outstationRequest) {
            return response()->json([ "success" => false, 'error_code' => 1, 'message' => "Your are not allowed to reject." ], 200);
        }

        $outstationRequest->status = 'rejected';
        $outstationRequest->save();

        return response()->json([ 'success' => true, 'message' => 'Outstation request rejected successfully.'], 200);

    }




    /** get outstation requests 
     * only those are bidding and pending state
     */
    public function getOutstationRequests(Request $request)
    {
        $records = OutstationBidding::where('status', 'pending')
            ->where('provider_id', $request->id)
            ->with([
                'outstationRequest' => function($query) {
                    $query->select([
                        'id', 'booking_id', 'source_address', 'source_latitude', 'source_longitude', 
                        'destination_address', 'destination_latitude', 'destination_longitude',
                        'total', 'status', 'trip_start_datetime', 'trip_end_datetime', 'is_round_trip',
                    ]);
                }
            ])
            ->get();

        $outstationRequests = [];
        foreach($records as $record) {
            $tempRecord = [
                'id' => $record->id,
                'booking_id' => $record->outstationRequest->booking_id,
                'source_address' => $record->outstationRequest->source_address,
                'source_latitude' => $record->outstationRequest->source_latitude,
                'source_longitude' => $record->outstationRequest->source_longitude,
                'destination_address' => $record->outstationRequest->destination_address,
                'destination_latitude' => $record->outstationRequest->destination_latitude,
                'destination_longitude' => $record->outstationRequest->destination_longitude,
                'status' => $record->status,
                'status_text' => $record->status_text,
                'total' => $record->outstationRequest->total,
                'round_trip' => $record->outstationRequest->is_round_trip,
                'start_date' => $record->outstationRequest->trip_start_datetime,
                'end_date' => $record->outstationRequest->trip_end_datetime
            ];


            $outstationRequests[] = $tempRecord;
        }

        return response()->json([ 'success' => true, 'message' => 'Outstation requests fetched', 'requests' => $outstationRequests ], 200);
        
    }



}
