<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Helper;
use App\Jobs\NormalPushNotification;
use Log;

class OutstationsAssign extends Command
{

    protected $signature = 'outstations:assign';
    protected $description = 'This command search all timeouts biddings and assign providers automaticaly';

   
    public function handle()
    {
        $this->log('Outstation assign process started');

        /** check auto settings enabled or not */
        if(!Helper::isOutstationAssignAuto()) {
            return $this->log('Auto assign process disabled.');
        }

        /** fetch timeout bidding outstation requests */
        $requests = Helper::fetchTimeoutOutstationRequests();
        
        /** loop through all requests, and assign provider */
        foreach($requests as $request) {
            $bidding = Helper::findBiddingForAutoAssign($request);

            if(!$bidding) {

                $this->log("No providers found for request : {$request->booking_id}");

                $request->status = 'no_provider_found'; //update request no provider found, and send push to user
                $request->save();

                dispatch( new NormalPushNotification($request->user_id, 0, "No Providers Found", "No providers found to accept your outstation booking id : {$request->booking_id}") );

                continue;
            }

            $this->log("Request id : {$request->booking_id}, assign provider : {$bidding->provider_id}");
            Helper::assiginBiddingToOutstationRequest($request, $bidding);

        }


        $this->log('Outstation assign process ended');

    }


    protected function log($data)
    {
        $this->info($data);
        Log::info($data);
    }



}
