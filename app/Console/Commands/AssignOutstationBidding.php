<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OutstationRequest;
use App\OutstationBidding;
use App\Helpers\Helper;
use Log;
use App\Jobs\NormalPushNotification;

class AssignOutstationBidding extends Command
{

    protected $signature = 'outstation:assignbidding';
    protected $description = 'Assgins provider for outstation request bidding';

    public function handle()
    {
        Log::info('outstation bidding assign process started');

        /** fetch pending outstation requests */
        $pendingRequests = Helper::getPendingOutstationRequests();
        
        /** for each request find nearby provider and assign for bidding */
        foreach($pendingRequests as $request) {
            $this->info("AssignOutstationBidding@assignProviderForBidding --> booking id  : {$request->booking_id}");
            Helper::assginProviderOutstationBidding($request);
        }
        
        Log::info('outstation bidding assign process ended');
    }


}
