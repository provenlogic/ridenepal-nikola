<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    
    /**
     * store all settings in memery for each reqeust
     * to make later query faster
     */
    protected static $settings = [];


    /**
     * fetch all settings when app boots up and loads Setting class
     */
    public static function init()
    {
        $settings = Settings::get();
        foreach($settings as $setting) {
            self::$settings[$setting->key] = $setting->value;
        }
    }


    /** 
     * get specific setting
     */
    public static function getByKey($key)
    {
        if( isset(self::$settings[$key]) ) {
            return self::$settings[$key];
        }
        
        $setting = Settings::where('key', $key)->first();
        self::$settings[$key] = $setting ? $setting->value : '';
        return self::$settings[$key];

    }



}

Settings::init();
