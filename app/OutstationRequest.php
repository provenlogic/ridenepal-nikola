<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutstationRequest extends Model
{
    protected $table = 'outstation_requests';
    protected $appends = [ 'status_text' ];

    //status --> pending, bidding, no_provider_found, user_canceled, driver_canceled, canceled,  driver_assigned, started, ended

    /** relationship with provider */
    public function provider()
    {
        return $this->belongsTo('App\Provider', 'provider_id');
    }


    /** relation with provider ratings table */
    public function providerRating()
    {
        return $this->hasOne('App\ProviderRating', 'request_id')->where('request_type', 'outstation');
    }



    /** relation with user ratings table */
    public function userRating()
    {
        return $this->hasOne('App\UserRating', 'request_id')->where('request_type', 'outstation');
    }




    /** relation with user */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    /** relation with biddings */
    public function biddings()
    {
        return $this->hasMany('App\OutstationBidding', 'outstation_request_id');
    }



    /** returns status text */
    public function getStatusTextAttribute()
    {
        if($this->status == 'pending') {
            return 'Pending';
        } else if($this->status == 'no_provider_found') {
            return 'No provider found';
        } else if($this->status == 'bidding') {
            return 'Bidding';
        } else if($this->status == 'driver_assigned') {
            return 'Confirmed';
        } else if($this->status == 'started') {
            return 'Outstation Started';
        } else if($this->status == 'ended') {
            return 'Completed';
        }  else if($this->status == 'user_canceled' || $this->status == 'driver_canceled') {
            return 'Canceled';
        } else if($this->status == 'provider_started') {
            return 'Provider on the way to pickup';
        } else  {
            return $this->status;
        }

    }


}
