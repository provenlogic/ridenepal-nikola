<?php

namespace App\Providers;
use App\AdminNotification;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.admin.header_notification', function ($view) {
            $notifications = AdminNotification::orderBy('created_at', 'desc')->orderBy('is_read', 'asc')->get();
            $unreadNotificationsCount = AdminNotification::where('is_read', 0)->count();
            $view->with('notifications', $notifications)->with('unread_notifications_count', $unreadNotificationsCount);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
