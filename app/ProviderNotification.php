<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderNotification extends Model
{
    protected $table = 'provider_notifications';
}
