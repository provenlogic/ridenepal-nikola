<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutstationLocationDestination extends Model
{
    protected $table = 'outstaion_location_destinations';

    /** relation with destination */
    public function destination()
    {
        return $this->belongsTo('App\OutstationLocation', 'destination_location_id');
    }


}
