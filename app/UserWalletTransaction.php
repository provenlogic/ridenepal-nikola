<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWalletTransaction extends Model
{
    protected $table = 'user_wallet_transactions';
}
