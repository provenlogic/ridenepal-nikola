<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutstationLocation extends Model
{
    protected $table = 'outstation_locations';


    /** relationship with destinations */
    public function destinations()
    {
        return $this->hasMany('App\OutstationLocationDestination', 'source_location_id');
    }


}
