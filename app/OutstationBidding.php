<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutstationBidding extends Model
{
    protected $table = 'outstation_biddings';
    protected $appends = ['status_text'];

    //status --> pending, accepted, rejected, assigned

    /** relation with outstation request */
    public function outstationRequest()
    {
        return $this->belongsTo('App\OutstationRequest', 'outstation_request_id');
    }

    /** relation with provider */
    public function provider()
    {
        return $this->belongsTo('App\Provider', 'provider_id');
    }


    /** returns status text virtual attribute */
    public function getStatusTextAttribute()
    {
        if($this->status == 'pending') {
            return 'Biddig Required';
        } if($this->status == 'accepted') {
            return 'Accepted';
        }
        if($this->status == 'rejected') {
            return 'Rejected';
        } if($this->status == 'assigned') {
            return 'Assigned';
        } else {
            return 'Pending';
        }
    }
    

}
